# OpenID Connect with Keycloak

Sample Postman request for obtaining an OIDC JWT from Keycloak

For more information on this sample Postman request, read my blog Get OpenID Connect tokens from Keycloak. 

In my blog you will learn how to setup Keycloak to issue a OIDC JWT for a client.